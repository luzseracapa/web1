<center>
<img src="<?php echo base_url(); ?>/assets/images/ang.jpg" alt="" height="300" width="300"> 
<img src="<?php echo base_url(); ?>/assets/images/108.jpg" alt="" height="300" width="300"> 
<img src="<?php echo base_url(); ?>/assets/images/103.jpg" alt="" height="300" width="300"> 
<img src="<?php echo base_url(); ?>/assets/images/r8dc.jpg" alt="" height="300" width="300"> 
</center>
<br>

<div class="row"
    style="color: black;padding: 20px;">
      <div class="col-md-4 text-center">
            <h4>
              <b style="color:red"> QUIENES SOMOS</b>
            </h4>
            <p>Somos una Familia apasionada por la gastronomía y 
                el buen servicio, que nos gusta rodearnos con gente 
                de los mismos gustos para disfrutar de atenderle de la mejor manera.
            </p>
      </div>
      <div class="col-md-4 text-center">
        <h4>
            <b style="color:red"> MISION</b>
        </h4>
        <p>Superar las expectativas de nuestros clientes de forma tal que 
           nuestro nombre sea conocido como una experiencia memorable.
           Ser opción destacable y diferente.
           Mantener una excelente calidad en nuestros platos.
        </p>
      </div>
      <div class="col-md-4 text-center">
        <h4>
            <b style="color:red"> VISION</b>
        </h4>
        <p>Ser reconocidos entre los mejores restaurantes a nivel local 
            y nacional por nuestra oferta gastronómica, ambiente y atención.
        </p>
      </div>
   </div>